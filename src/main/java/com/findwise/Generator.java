package com.findwise;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;

/**
 * Created by esp on 9/2/16.
 */
public class Generator {

    public static void main(String args[]) throws IOException {
        XSSFWorkbook wb = XlsxReader.readWorkbook(Generator.class.getResourceAsStream("/test.xlsx"));
        SheetModel model = XlsxReader.readXLSXFile(wb, 0);
        EnumWriter enumWriter = new EnumWriter(model, "com.findwise", "gensrc", 0);
        enumWriter.write();
    }
}
