package com.findwise;

import java.util.List;
import java.util.Map;

/**
 * Created by esp on 9/2/16.
 */
public class SheetModel {
    private final Map<Integer, String> headers;
    private final List<String[]> values;
    private final String sheetName;

    public SheetModel(Map<Integer, String> headers, List<String[]> values, String sheetName) {
        this.headers = headers;
        this.values = values;
        this.sheetName = sheetName;
    }

    public List<String[]> getValues() {
        return values;
    }

    public Map<Integer, String> getHeaders() {
        return headers;
    }

    public String getSheetName() {
        return sheetName;
    }
}
