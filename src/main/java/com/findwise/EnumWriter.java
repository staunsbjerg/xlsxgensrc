package com.findwise;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Iterator;
import java.util.Map;
import java.util.regex.Matcher;

/**
 * Created by esp on 9/2/16.
 */
public class EnumWriter {
    private final SheetModel model;
    private final String packageName;
    private final String srcDir;
    private final String enumName;
    private final String separator = System.getProperty("file.separator", "/");
    private static final String INDENT = "    ";
    private final int identifierIndex;

    public EnumWriter(SheetModel model, String packageName, String srcDir, int identifierIndex) {
        this.model = model;
        this.packageName = packageName;
        this.srcDir = srcDir;
        this.enumName = sheetName2JavaName(model.getSheetName());
        this.identifierIndex = identifierIndex;
    }

    public void write() throws IOException {
        String filename  = srcDir + separator + package2Dir(packageName) + separator + enumName + ".java";
        File file = new File(filename);
        if (!file.getParentFile().exists()) {
            file.getParentFile().mkdirs();
        }
        file.createNewFile();
        Writer writer = new FileWriter(file); //Files.newBufferedWriter(file, Charset.forName("UTF-8"));

        writePackage(writer);
        writeClassStart(writer);

        writeBody(writer);

        writeClassEnd(writer);

        writer.close();
    }

    private void writeBody(Writer writer) throws IOException {
        writeConstants(writer);

        for (Map.Entry<Integer, String> headerEntry : model.getHeaders().entrySet()) {
            String identifier = name2JavaIdentifier(headerEntry.getValue());
            writeField(writer, identifier);
        }

        for (Map.Entry<Integer, String> headerEntry : model.getHeaders().entrySet()) {
            String identifier = name2JavaIdentifier(headerEntry.getValue());
            writeGetter(writer, identifier);
        }

        writeCtor(writer);

    }

    private void writeConstants(Writer writer) throws IOException {
        Iterator<String[]> values = model.getValues().iterator();
        while (values.hasNext()) {
            String[] valueRow = values.next();
            indent(writer);
            String constantName = name2JavaIdentifier(valueRow[identifierIndex]).toUpperCase();
            writer.write(constantName);
            writer.write(" (");
            for (int i = 0; i < valueRow.length; i++) {
                writer.write("\"");
                writer.write(valueRow[i]);
                writer.write("\"");
                if (i + 1 < valueRow.length) {
                    writer.write(", ");
                }
            }
            writer.write(")");
            if (values.hasNext()) {
                writer.write(",");
            } else {
                writer.write(";");
            }
            writer.write("\n");
        }
        writer.write("\n");
    }

    private void writeCtor(Writer writer) throws IOException {
        indent(writer);
        writer.write("private ");
        writer.write(enumName);
        writer.write(" (");
        Iterator<Map.Entry<Integer, String>> headers = model.getHeaders().entrySet().iterator();
        if (headers.hasNext()) {
            while (true) {
                Map.Entry<Integer, String> headerEntry = headers.next();
                writer.write("String ");
                writer.write(name2JavaIdentifier(headerEntry.getValue()));
                if (headers.hasNext()) {
                    writer.write(", ");
                    continue;
                }
                break;
            }
        }
        writer.write(") {\n");

        for (Map.Entry<Integer, String> headerEntry : model.getHeaders().entrySet()) {
            String identifier = name2JavaIdentifier(headerEntry.getValue());
            indent(writer);
            indent(writer);
            writer.write("this.");
            writer.write(identifier);
            writer.write(" = ");
            writer.write(identifier);
            writer.write(";\n");
        }
        indent(writer);
        writer.write("}\n");
    }

    private void writeGetter(Writer writer, String name) throws IOException{
        indent(writer);
        writer.write("public String ");
        writer.write(name);
        writer.write("() {\n");
        indent(writer);
        indent(writer);
        writer.write("return ");
        writer.write(name);
        writer.write(";\n");
        indent(writer);
        writer.write("}\n");
    }

    private void indent(Writer writer) throws IOException {
        writer.write(INDENT);
    }

    private String name2JavaIdentifier(String value) {
        char[] name = value.toCharArray();
        if (!Character.isJavaIdentifierStart(name[0])) {
            name[0] = '_';
        }
        for (int i = 1; i < name.length; i++) {
            if (Character.isJavaIdentifierPart(name[i])) {
                continue;
            }

            // replacement character
            name[i] = '_';
        }
        return new String(name);
    }

    private void writeField(Writer writer, String name) throws IOException{
        indent(writer);
        writer.write("private final String ");
        writer.write(name);
        writer.write(";\n");
    }

    private String sheetName2JavaName(String sheetName) {
        char[] name = sheetName.toCharArray();
        name[0] = Character.toUpperCase(name[0]);
        for (int i = 1; i < name.length; i++) {
            if (Character.isJavaIdentifierPart(name[i])) {
                continue;
            }

            // replacement character
            name[i] = '_';

        }
        String result = new String(name);
        return Character.isJavaIdentifierStart(name[0]) ? result : "Gen" + result;
    }

    private String package2Dir(String packageName) {
        return packageName.replaceAll("\\.", Matcher.quoteReplacement(separator));
    }

    private void writePackage(Writer writer) throws IOException {
        writer.write("package ");
        writer.write(packageName);
        writer.write(";\n\n");
    }
    private void writeClassStart(Writer writer) throws IOException {
        writer.write("public enum ");
        writer.write(enumName);
        writer.write(" {\n");
    }
    private void writeClassEnd(Writer writer) throws IOException {
        writer.write("}\n");
    }
}
