package com.findwise;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Created by esp on 9/2/16.
 */
public class XlsxReader {

    public static XSSFWorkbook readWorkbook(InputStream stream) throws IOException {
        return new XSSFWorkbook(stream);
    }

    public static SheetModel readXLSXFile(XSSFWorkbook wb, int sheetIndex) throws IOException {
        XSSFSheet sheet = wb.getSheetAt(sheetIndex);

        XSSFRow row;
        XSSFCell cell;
        Map<Integer, String> headings = new HashMap<Integer, String>();

        Iterator<Row> rows = sheet.rowIterator();
        // read headers
        if (!rows.hasNext()) {
            throw new RuntimeException("No rows in sheet!");
        }
        XSSFRow header = (XSSFRow) rows.next();
        Iterator<Cell> headerCells = header.cellIterator();
        int headerIndex = 0;
        while (headerCells.hasNext()) {
            XSSFCell headerCell = (XSSFCell) headerCells.next();
            String heading = headerCell.getStringCellValue();

            headings.put(headerIndex, heading);

            headerIndex++;
        }
        final int maxCellCount = headerIndex;

        // read values
        List<String[]> values = new ArrayList<String[]>();
        while (rows.hasNext()) {
            int cellPos = 0;
            String[] cellValues = new String[maxCellCount];
            row = (XSSFRow) rows.next();
            Iterator cells = row.cellIterator();
            while (cells.hasNext() && cellPos < maxCellCount) {
                cell = (XSSFCell) cells.next();
                cellValues[cellPos] = cell.getStringCellValue();
                cellPos++;
            }
            values.add(cellValues);
        }
        return new SheetModel(headings, values, sheet.getSheetName());
    }
}
